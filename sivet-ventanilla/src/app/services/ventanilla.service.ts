import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class VentanillaService {

  constructor( private http : HttpClient) { }

  private _headers = new HttpHeaders({ 'content-type' : 'application/json' })
  private _urlVentanilla : string = 'http://192.168.254.3:3001'


  public obtenerTurnos( params : object ): Observable<any> { 
    return this.http.post<any>(this._urlVentanilla + "/ventanillas/tickets/obtener" , params, {'headers':this._headers})
    .pipe(
      map((resp: any) => { 
        return resp;
      }),
      catchError(err => {
        console.log(err)  
          return err;
          })
      )
  }

  public cargarAreasVentanillas() : Observable<any>{
      return this.http.post(this._urlVentanilla + "/administracion/configuraciones/obtenerVentanilla", {"id":"ventanillaCategoria"} ,{'headers': this._headers})
      .pipe(
          map((resp: any) => {          
          if (resp) {
            console.log(resp);
            return resp.data;
          } else {
            return null
          }
      }),
      catchError((err) => {
          return err ;
      })
    );
  }
  
  public cargarAreasAtencion() : Observable<any>{
      return this.http.post(this._urlVentanilla + "/administracion/catalogos/obtener", {"tipo":"areasAtencion"} ,{'headers': this._headers})
      .pipe(
          map((resp: any) => {          
          if (resp) {
            console.log(resp);
            return resp.data;
          } else {
            return null
          }
      }),
      catchError((err) => {
          return err ;
      })
    );
  }

  public asignarTicketVentanilla( params : object) : Observable<any>{
    return this.http.post(this._urlVentanilla + "/ventanillas/tickets/asignar", params ,{'headers': this._headers})
      .pipe(
          map((resp: any) => {          
          if (resp) {
            console.log(resp);
            return resp.data[0];
          } else {
            return null
          }
      }),
      catchError((err) => {
          return err ;
      })
    );
  }
  
  public finalizarTicket( params : object) : Observable<any>{
    return this.http.post(this._urlVentanilla + "/ventanillas/tickets/finalizar", params ,{'headers': this._headers})
      .pipe(
          map((resp: any) => {          
          if (resp) {
            console.log(resp);
            return resp.data;
          } else {
            return null
          }
      }),
      catchError((err) => {
          return err ;
      })
    );
  }

}
