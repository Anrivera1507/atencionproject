import { SegOpcionesModel } from './../models/seg_opciones';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ResponseModel } from '../models/responseModel';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class OpcionService {

  urlAPI        = environment.urlAPI;
  headers = new HttpHeaders({ 'content-type' : 'application/json' })    

  constructor(
    private _http   :    HttpClient,
    private _config :    ConfigService
  ) { }

  public guardar(opcion: SegOpcionesModel): Observable<ResponseModel> {
    return this._http.post<ResponseModel>(environment.urlAPI + '/seguridad/opciones/guardar', opcion , {'headers' : this._config.headers})
    .pipe(
          map((resp: ResponseModel) => {
              return resp.data;
          }),
          catchError(err => {
              console.log(err)
              return err;
          })
        )
  }

  public obtener(id: number): Observable<ResponseModel> {
    return this._http.post<ResponseModel>(environment.urlAPI + '/seguridad/opciones/obtener', { "id" : id }, {'headers' : this._config.headers})
    .pipe(
          map((resp: ResponseModel) => {
              return resp.data;
          }),
          catchError(err => {
              console.log(err)
              return err;
          })
        )
  }

  public obtenerPorModuloYRol(idModulo?: number, idRol?: number): Observable<ResponseModel> {
    return this._http.post<ResponseModel>(environment.urlAPI + '/seguridad/opciones-rol/obtener', { "idModulo" : idModulo, "idRol" : idRol }, {'headers' : this._config.headers})
    .pipe(
          map((resp: ResponseModel) => {
              return resp.data;
          }),
          catchError(err => {
              console.log(err)
              return err;
          })
        )
  }
  
}
