export interface AreasAtencion {
    id              : string,
    idRelacion      : string,
    tipo            : string,
    codigo          : string,
    descripcion     : string,
    creadoEn        : string,
    creadoPor       : string,
    modificadoEn    : string,
    modificadoPor   : string,
    activo          : string
}