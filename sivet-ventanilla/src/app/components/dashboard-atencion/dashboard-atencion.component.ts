import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { TicketModel } from 'src/app/models/tickets.model';
import { UserService } from 'src/app/services/user.service';
import { VentanillaService } from 'src/app/services/ventanilla.service';
import { NotSubtractHoursDate } from 'src/app/utils/constantes';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-dashboard-atencion',
  templateUrl: './dashboard-atencion.component.html',
  styleUrls: ['./dashboard-atencion.component.scss']
})
export class DashboardAtencionComponent implements OnInit, AfterViewInit {


  constructor(
    private sanitizer : DomSanitizer,
    private _user     : UserService,
    private _turnoSer : VentanillaService
  )
  { }


  idArea      : string = ''
  // urlRegistro =  environment.urlRegistro;
  urlRegistro : any = ''
  urlBecas    : any = ''
  urlCreditos : any = ''
  urlPagos    : any = ''
  display     : boolean = false
  dataVentanillas : any[] = [];
  ventanillas     : any[]
  ticketLlamada   : TicketModel 
  lsTicket     : TicketModel[] = []
  lsTicketSinRespuesta     : TicketModel[] = []

  paramTickets : object = {
    "pendiente" : true,
    "idArea"    : localStorage.getItem('IdArea')
  }

  listaTickets : TicketModel[] = []

  ngOnInit(
  ): void {
    this.urlPagos = this.sanitizer.bypassSecurityTrustResourceUrl(environment.urlPagos)
    this.urlBecas = this.sanitizer.bypassSecurityTrustResourceUrl(environment.urlBecas)
    this.urlCreditos = this.sanitizer.bypassSecurityTrustResourceUrl(environment.urlCreditos)
    this.urlRegistro = this.sanitizer.bypassSecurityTrustResourceUrl(environment.urlRegistro)
    if(localStorage.getItem('IdArea')){
      this.cargarTurnosVentanilla( this.paramTickets, 1)
    }
  }
  
  refrescar( e ){
    console.log(e);
    // this.urlCreditos = this.sanitizer.bypassSecurityTrustResourceUrl(environment.urlCreditos)
  }
  
  ngAfterViewInit(): void {
    this._user.refreshIframe( environment.urlPagos, "ifPagos");
    this._user.refreshIframe( environment.urlBecas, "ifBecas");
    this._user.refreshIframe( environment.urlCreditos, "ifCreditos");
    this._user.refreshIframe( environment.urlRegistro, "ifRegistro");
    
    
  }

  cargarTurnosVentanilla( param : object, tipoLista : number){   
    this._turnoSer.obtenerTurnos( param ).subscribe(
      resp => {
        console.log(resp.data);
        if( resp.data != null && resp.data.length > 0){
          this.listaTickets = resp.data
          this.listaTickets.forEach(ticket => {
            ticket.creadoEn = NotSubtractHoursDate(ticket.creadoEn.toString())
          });
        }
      },
      (err) => {
        console.log('Error:');
        console.log(err);
      }
    )
  }

  cargarVentanillasAreas(){
    this._turnoSer.cargarAreasVentanillas().subscribe(
       resp => {
        console.log(resp);
        this.dataVentanillas = resp.data
        this.display = true
      },
      (err) => {
        console.log('Error:');
        console.log(err);
      }
    )
  }
  
  llamarSinRespuesta(ticket : TicketModel){
    if( this.listaTickets.length > 0){
      let parametros = {
        "id" : ticket.id,
        "idTecnico": localStorage.getItem('idUser'),
        "escritorio": 5,
        "modificadoEn": new Date(),
        "modificadoPor": localStorage.getItem('idUser'),
        "idArea" : localStorage.getItem('IdArea'),
        "activo": true
      }
      this._turnoSer.asignarTicketVentanilla( parametros ).subscribe(
        resp => {
          console.log(resp);
          this.cargarTurnosVentanilla( this.paramTickets, 2)
        },
        (err) => {
          console.log('Error:');
          console.log(err);
        }
      );
      
    }
  }
  
  llamada(){
    if( this.listaTickets.length > 0){
      console.log( this.listaTickets );
      let parametros = {
        "id" : this.listaTickets[0].id,
        "idTecnico": localStorage.getItem('idUser'),
        "escritorio": 5,
        "modificadoEn": new Date(),
        "modificadoPor": localStorage.getItem('idUser'),
        // "idArea" : '14070',
        "idArea" : localStorage.getItem('IdArea'),
        "activo": true
      }
      this._turnoSer.asignarTicketVentanilla( parametros ).subscribe(
        ticket => {
          console.log(ticket);
            this.cargarTurnosVentanilla( this.paramTickets, 1)
        },
        (err) => {
          console.log('Error:');
          console.log(err);
        }
      );
    }else{

    }
  }
  
}
