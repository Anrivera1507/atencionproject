import { Component, OnInit, AfterViewInit } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { AppComponent } from 'src/app/app.component';
import { MenuService } from 'src/app/app.menu.service';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { VentanillaService } from '../../services/ventanilla.service';
import { AreaModel, AreaVentanillaModel, TicketModel } from '../../models/tickets.model';
import { NotSubtractHoursDate } from 'src/app/utils/constantes';
import { AreasAtencion } from 'src/app/interfaces/areas.interface';
import Swal from 'sweetalert2';
import { Socket } from 'ngx-socket-io';
@Component({
  selector: 'app-atencion-veterano',
  templateUrl: './atencion-veterano.component.html',
  styleUrls: ['./atencion-veterano.component.scss']
})
export class AtencionVeteranoComponent implements OnInit, AfterViewInit  {
  urlRegistro = this.sanitizer.bypassSecurityTrustResourceUrl(environment.urlRegistro)
  
  itemBtn : object [] = []

  idArea        : string = ''
  numVentanilla : string = ''
  listaTicket   : TicketModel [] = []
  codigoArea    : AreaModel [] = [];
  idAreaSelect  : AreaModel
  // codigoArea    : AreaVentanillaModel [] = [];
  // idAreaSelect  : AreaVentanillaModel
  display       : boolean = false
  listaVentanillas : string [] = []
  // lsVentanillas : string [] = []
  lsVentanillas : object [] = []
  idVentanilla  : string = ''
  
  constructor(
    public app          : AppComponent, 
    private sanitizer   : DomSanitizer,
    private _user       : UserService,
    private router      : Router,
    private ventanilla  : VentanillaService,
    private socket      : Socket
    ) { }
    param : object = {
      'pendiente' : false,
      'idArea'    : '14070'
    }

    ngOnInit(): void {
      // this.iniciarSocket()
      if( !localStorage.getItem('IdArea')){
        this.cargarAreasAtencion()
      }
      this.itemBtn = [
        {label: 'Salir', icon: 'pi pi-sign-out', command: ( resp=> 
          {
            localStorage.clear();
            sessionStorage.clear();
            this.router.navigate(["/login"]);
          }) 
        }
      ]
    }
  
  ngAfterViewInit(): void {
    this.urlRegistro = this.sanitizer.bypassSecurityTrustResourceUrl(environment.urlRegistro)
    setTimeout(() => {
      this._user.refreshIframe( environment.urlRegistro, "ifRegistro" )
    }, 1000);
    if(localStorage.getItem('IdArea')){
      this.cargarTurnos(this.param)
    }
  }
  
  llamarSinRespuesta(ticket : TicketModel){
    if( this.listaTicket.length > 0){
      let parametros = {
        "id" : ticket.id,
        "idTecnico": localStorage.getItem('idUser'),
        "escritorio": 5,
        "modificadoEn": new Date(),
        "modificadoPor": localStorage.getItem('idUser'),
        "activo": true
      }
      this.ventanilla.asignarTicketVentanilla( parametros ).subscribe(
        resp => {
          console.log(resp);
        },
        (err) => {
          console.log('Error:');
          console.log(err);
        }
      );
    }
  }

  cargarTurnos( param ){
    this.ventanilla.obtenerTurnos( param ).subscribe(
    resp => {
          console.log('Lista de turnos pendientes');
          console.log(resp.data);
          if( resp.data != null && resp.data.length > 0){
            this.listaTicket = resp.data
            this.listaTicket.forEach(ticket => {
              ticket.creadoEn = NotSubtractHoursDate(ticket.creadoEn.toString())
            });
          }
        },
        (err) => {
          console.log('Error:');
          console.log(err);
        }
    )
  }

    llamada(){
      if( this.listaTicket.length > 0){
        console.log( this.listaTicket );
        let parametros = {
          "id"            : this.listaTicket[0].id,
          "idTecnico"     : localStorage.getItem('idUser'),
          "escritorio"    : 5,
          "modificadoEn"  : new Date(),
          "modificadoPor" : localStorage.getItem('idUser'),
          "idArea"        : localStorage.getItem('IdArea'),
          "activo"        : true
        }
        this.ventanilla.asignarTicketVentanilla( parametros ).subscribe(
          ticket => {
            console.log(ticket);
              this.cargarTurnos(this.param)
          },
          (err) => {
            console.log('Error:');
            console.log(err);
          }
        );
      }
    }
    
    finalizarTicket( tictet : TicketModel ){
      let parametros = {
          "id": tictet.id,
          "modificadoPor": localStorage.getItem('idUser')
      }
      this.ventanilla.asignarTicketVentanilla( parametros ).subscribe(
        resp => {
          console.log(resp);
          if( resp[0].numeroLlamada == 3 ){
            this.cargarTurnos(this.param)
          }
        },
        (err) => {
          console.log('Error:');
          console.log(err);
        }
      );
    }

    cargarAreasAtencion(){
      if( !localStorage.getItem('IdArea')){
        this.ventanilla.cargarAreasAtencion().subscribe(
           resp => {
            this.codigoArea = resp
            this.display = true
          },
          (err) => {
            console.log('Error:');
            console.log(err);
          }
        )
      }
    }

    cargarVentanillasArea(){
      this.ventanilla.cargarAreasVentanillas().subscribe(
        (lista) =>{
          this.codigoArea = lista
          this.display = true
        },
        (err) =>{
          console.log(err);
          
        }
      )
    }

    actualizarDpVentanillas( e : AreaModel) {
      console.log(e);
      // this.lsVentanillas = e;
      this.cargarVentanillasArray(5)
    }

    guardarVentanillaArea(numVentanilla : any, areaSelect : AreaModel){
      console.log(numVentanilla);
      console.log(areaSelect);
      
      if( areaSelect.id != '' ){
        localStorage.setItem('IdArea', areaSelect.id);
        localStorage.setItem('numVentanilla', (numVentanilla.id + 1));
        this.param = {
          'pendiente' : false,
          'idArea'    : localStorage.getItem('IdArea')
        }
        // this.cargarTurnos(this.param)
        this.display = false
        this.reloadCurrentRoute()
      }else{
        Swal.fire({
          title:'Aviso',
          text:'Por favor seleccione un area de atencion',
          icon:'warning',
          confirmButtonText:'Entendido',
          confirmButtonColor: '#3085D6'
        }).then(function (result)
        {

        })
      }
    }

    cargarVentanillasArray( cuenta : number){
      for (let index = 0; index < cuenta; index++) {
        this.lsVentanillas.push({
          id: index, descripcion : `Ventanilla ${(index + 1)}`
        })
      }
      console.log(this.lsVentanillas);
      
    }

    iniciarSocket(){
      // debugger
      this.socket.connect()
      console.log('conectando');
    }
    
    reloadCurrentRoute() {
      let currentUrl = this.router.url;
      console.log(currentUrl);
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate([currentUrl]);
    }
}
