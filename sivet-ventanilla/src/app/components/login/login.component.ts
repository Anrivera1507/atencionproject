import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuService } from 'src/app/app.menu.service';
import { UserService } from 'src/app/services/user.service';
import {DialogModule} from 'primeng/dialog';
import Swal from 'sweetalert2';
import { AreasAtencion } from 'src/app/interfaces/areas.interface';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  constructor(
    public router: Router,
    public _user: UserService
    ) { }
    
    userObj = {
      user          : '',
      password      : '',
      areasAtencion : []
    }
    areas         : AreasAtencion [];
    // selectedAreas : AreasAtencion [];
    selectedAreas : string = ""

    ventanillas : object [] = [
      { id : '1', codigo : "" },
      { id : '2', codigo : "" },
      { id : '7', codigo : "" },
      { id : '6', codigo : "" },
      { id : '5', codigo : "" },
    ]

    ventanillaSelet : string = '';
    display: boolean;

  ngOnInit(): void {
    this.obtenerAreasAtencion()

  }
  
  authenticate(){
    this._user.authentication(this.userObj.user, this.userObj.password, this.selectedAreas, this.ventanillaSelet).subscribe(resp => {
      console.log(resp);
      console.log(this.selectedAreas);
      if (resp) {
        this.router.navigate(["/"]);
        Swal.fire({
          title:'Bienvenid@!',
          text: resp.usuario,
          icon:'success',
          confirmButtonText:'Entendido',
          confirmButtonColor: 'darkgreen'
        }).then(function (result)
        {})
      } else {
          Swal.fire({
            title:'Aviso',
            text:'Usuario o contraseña inválidos',
            icon:'warning',
            confirmButtonText:'Entendido',
            confirmButtonColor: '#3085D6'
          }).then(function (result)
          {})
      }
    } );
  }

  obtenerAreasAtencion(){
    this._user.obtenerAreasAtencion().subscribe(
      (resp) =>{
        this.areas = resp
        console.log(this.areas);
      },
      (err) => {
        console.log(err);
      }
    )
  }

  // showModalDialog() {
  //   this.displayModal = true;
  // }
  
}
