import {Component, OnInit} from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
})
export class AppComponent implements OnInit{

    //topbarColor = 'layout-topbar-blue';
    topbarColor = 'layout-topbar-inabve';

    menuColor = 'layout-menu-light';

    themeColor = 'blue';

    layoutColor = 'blue';

    topbarSize = 'large';

    horizontal = true;

    inputStyle = 'outlined';

    ripple = true;

    compactMode = false;

    constructor(private primengConfig: PrimeNGConfig) { }

    ngOnInit() {
        this.primengConfig.ripple = true;
        // window.addEventListener('message', function(event) {
        //     sessionStorage.clear()
        //     var origin = event.origin //|| event.originalEvent.origin; // For Chrome, the origin property is in the event.originalEvent object.
        //     if (origin == "")
        //     return;
        //     if (origin.includes(environment.urlServer)) {
        //         let data = JSON.parse(event.data)
        //         sessionStorage.setItem("token", data.token)
        //         sessionStorage.setItem("user", data.user)
        //         sessionStorage.setItem("idUser", data.idUser)
        //     }
        // }, false);
    }
}
