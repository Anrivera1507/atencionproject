export class TicketModel
{
    id              : string = ''
    idEstado        : string = ''
    idArea          : string = ''
    idTecnico       : string = ''
    idVeterano      : string = ''
    codigo          : string = ''
    numeroLlamada   : string = ''
    fechaInicio     : string = ''
    fechaFin        : string = ''
    escritorio      : string = ''
    creadoEn        : Date
    creadoPor       : string = ''
    modificadoEn    : string = ''
    modificadoPor   : string = ''
    activo          : string = ''
    nombreCompleto  : string = ''
    carnet          : string = ''
    estado          : string = ''
    area            : string = ''
}

export class AreaModel {
    id              : string = ''
    idRelacion      : string = ''
    tipo            : string = ''
    codigo          : string = ''
    descripcion     : string = ''
    creadoEn        : string = ''
    creadoPor       : string = ''
    modificadoEn    : string = ''
    modificadoPor   : string = ''
    activo          : string
}

export class AreaVentanillaModel {
    cantidad    : string = ''
    nombre_area : string = ''
    ventanillas : string [] = []
}