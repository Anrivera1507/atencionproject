export class VetVeteranosModel
{
    indice: number;
    primerNombre: string | null;
    segundoNombre: string | null;
    primerApellido: string | null;
    segundoApellido: string | null;
    nombreCompleto: string | null;
    sexo: number | null;
    fechaNacimiento: Date | null;
    codigoCanton: string | null;
    nombreDepartamento: string | null;
    nombreMunicipio: string | null;
    nombreCanton: string | null;
    nombreCaserioColonia: string | null;
    dui: string | null;
    nit: string | null;
    nisNic: string | null;
    telefonoFijo: string | null;
    telefonoCelular: string | null;
    email: string | null;
    fechaEntrevista: Date | null;
    resultadoEntrevista: number | null;
    informanteVeterano: number | null;
    otroGradoMilitar: string | null;
    fechaIncorporacionRevolucion: Date | null;
    seudonimo: string | null;
    codigoDepartamentoRevolucion: string | null;
    codigoDepartamentoDesmoviliza: string | null;
    fueDesmovilizado: number | null;
    fechaDesmovilizacion: Date | null;
    numeroCarnetDesmovilizado: string | null;
    estadoCarnetDesmovilizado: number | null;
    motivoNoDesmovilizado: string | null;
    nombreJefe_1: string | null;
    seudonimoJejfe_1: string | null;
    nombreJefe_2: string | null;
    seudonimoJefe_2: string | null;
    campoNombre_1: string | null;
    campoSeudonimo_1: string | null;
    campoNombre_2: string | null;
    campoSeudonimo_2: string | null;
    campoNombre_3: string | null;
    campoSeudonimo_3: string | null;
    estaPensionado: number | null;
    pensionadoMinisterioHacienda: number | null;
    dcivilFecInicioPeriodo1: Date | null;
    dcivilFecFinalPeriodo_1: Date | null;
    dcivilFecInicioPeriodo2: Date | null;
    dcivilFecFinalPeriodo2: Date | null;
    dcivilFecInicioPeriodo3: Date | null;
    dcivilFecFinalPeriodo3: Date | null;
    ultimoDepartamento: string | null;
    ultimoMunicipio: string | null;
    ultimoCaserio: string | null;
    ultimoBarrio: string | null;
    ultimaColonia: string | null;
    fueTurnero: number | null;
    alguienRecibePensionIpsfa: number | null;
    nombreRecibePensionIpsfa: string | null;
    nombreVeteranoFallecido: string | null;
    numeroIpsfa: string | null;
    ultimaDireccionDomicilioActual: string | null;
    numeroConstanciaAlta: string | null;
    fechaEmisionConstanciaAlta: Date | null;
    unidadExtendioConstanciaAlta: string | null;
    numeroConstanciaBaja: string | null;
    fechaEmisionConstanciaBaja: Date | null;
    unidadExtendioConstanciaBaja: string | null;
    numeroCertificacionPartidaDefuncion: string | null;
    fechaEmisionPartidaDefuncion: Date | null;
    unidadExtendioPartidaDefuncion: string | null;
    numeroCertificacionPartidaDefuncion2: string | null;
    fechaEmisionPartidaDefuncion2: Date | null;
    alcaldiaExtendioPartidaDefuncion: string | null;
    codigoMilitarPertenecio: number | null;
    padeceTrauma: number | null;
    recibidoAtencionEmocional: number | null;
    institucionAtencionEmocional: number | null;
    otroEmocional: string | null;
    beneficioConProgramas: number | null;
    lisiadoGuerra: number | null;
    asistenciaPorLisiado: number | null;
    codigoUsuario: string | null;
    fechaIngreso: Date | null;
    familiarExtranjero: number | null;
    recibeRemesas: number | null;
    montoRemesas: number | null;
    cantidadHombres: number | null;
    cantidadMujeres: number | null;
    observaciones: string | null;
    verificado: number | null;
    noCarnet: number | null;
    censo: string | null;

    constructor()
    {
        this.indice = 0
        this.primerNombre = null
        this.segundoNombre = null
        this.primerApellido = null
        this.segundoApellido = null
        this.nombreCompleto = null
        this.sexo = null
        this.fechaNacimiento = null
        this.codigoCanton = null
        this.nombreDepartamento = null
        this.nombreMunicipio = null
        this.nombreCanton = null
        this.nombreCaserioColonia = null
        this.dui = null
        this.nit = null
        this.nisNic = null
        this.telefonoFijo = null
        this.telefonoCelular = null
        this.email = null
        this.fechaEntrevista = null
        this.resultadoEntrevista = null
        this.informanteVeterano = null
        this.otroGradoMilitar = null
        this.fechaIncorporacionRevolucion = null
        this.seudonimo = null
        this.codigoDepartamentoRevolucion = null
        this.codigoDepartamentoDesmoviliza = null
        this.fueDesmovilizado = null
        this.fechaDesmovilizacion = null
        this.numeroCarnetDesmovilizado = null
        this.estadoCarnetDesmovilizado = null
        this.motivoNoDesmovilizado = null
        this.nombreJefe_1 = null
        this.seudonimoJejfe_1 = null
        this.nombreJefe_2 = null
        this.seudonimoJefe_2 = null
        this.campoNombre_1 = null
        this.campoSeudonimo_1 = null
        this.campoNombre_2 = null
        this.campoSeudonimo_2 = null
        this.campoNombre_3 = null
        this.campoSeudonimo_3 = null
        this.estaPensionado = null
        this.pensionadoMinisterioHacienda = null
        this.dcivilFecInicioPeriodo1 = null
        this.dcivilFecFinalPeriodo_1 = null
        this.dcivilFecInicioPeriodo2 = null
        this.dcivilFecFinalPeriodo2 = null
        this.dcivilFecInicioPeriodo3 = null
        this.dcivilFecFinalPeriodo3 = null
        this.ultimoDepartamento = null
        this.ultimoMunicipio = null
        this.ultimoCaserio = null
        this.ultimoBarrio = null
        this.ultimaColonia = null
        this.fueTurnero = null
        this.alguienRecibePensionIpsfa = null
        this.nombreRecibePensionIpsfa = null
        this.nombreVeteranoFallecido = null
        this.numeroIpsfa = null
        this.ultimaDireccionDomicilioActual = null
        this.numeroConstanciaAlta = null
        this.fechaEmisionConstanciaAlta = null
        this.unidadExtendioConstanciaAlta = null
        this.numeroConstanciaBaja = null
        this.fechaEmisionConstanciaBaja = null
        this.unidadExtendioConstanciaBaja = null
        this.numeroCertificacionPartidaDefuncion = null
        this.fechaEmisionPartidaDefuncion = null
        this.unidadExtendioPartidaDefuncion = null
        this.numeroCertificacionPartidaDefuncion2 = null
        this.fechaEmisionPartidaDefuncion2 = null
        this.alcaldiaExtendioPartidaDefuncion = null
        this.codigoMilitarPertenecio = null
        this.padeceTrauma = null
        this.recibidoAtencionEmocional = null
        this.institucionAtencionEmocional = null
        this.otroEmocional = null
        this.beneficioConProgramas = null
        this.lisiadoGuerra = null
        this.asistenciaPorLisiado = null
        this.codigoUsuario = null
        this.fechaIngreso = null
        this.familiarExtranjero = null
        this.recibeRemesas = null
        this.montoRemesas = null
        this.cantidadHombres = null
        this.cantidadMujeres = null
        this.observaciones = null
        this.verificado = null
        this.noCarnet = null
        this.censo = null
    }
}