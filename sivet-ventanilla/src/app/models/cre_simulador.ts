export class CreSimulador
{
	numPago: number;
    fechaPago: Date;
    saldoInicial: number;
    cuota: number;
    capital: number;
    interes: number;
    fpm: number;
    saldoFinal: number;

	constructor()
	{
        this.numPago = 0;
        this.fechaPago = new Date();
        this.saldoInicial = 0
        this.cuota = 0
        this.capital = 0
        this.interes = 0
        this.fpm = 0
        this.saldoFinal = 0
	}
}