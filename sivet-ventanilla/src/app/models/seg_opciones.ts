export class SegOpcionesModel
{
	id : number
	idModulo : number
	idRol : number
	idPadre : number
	nombre : string
	descripcion : string
	icono : string | null
	url : string
	activo : number
	creadoPor : number
	creadoEn : Date
	modificadoPor : number
	modificadoEn : Date

	constructor()
	{
		this.id = -1
		this.idModulo = -1
		this.idRol = -1
		this.idPadre = -1
		this.nombre = ''
		this.descripcion = ''
		this.icono = ''
		this.url = ''
		this.activo = 1
		this.creadoPor = -1
		this.creadoEn = new Date()
		this.modificadoPor = -1
		this.modificadoEn = new Date()
	}
}