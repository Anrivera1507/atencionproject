import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/misc/guards/auth.guard'


const routes: Routes = [
  {
    path: 'home',
    redirectTo: '',
    pathMatch: 'full'
  },
  {
    path: 'beneficios-listado',
    redirectTo: '',
    pathMatch: 'full'
  },
  {
    path: '',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'beneficios-listado',
    loadChildren: () => import('./pages/beneficios-listado/beneficios-listado.module').then( m => m.BeneficiosListadoPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'beneficios-detalle',
    loadChildren: () => import('./pages/beneficios-detalle/beneficios-detalle.module').then( m => m.BeneficiosDetallePageModule)
  },
  {
    path: 'impresion-ticket',
    loadChildren: () => import('./pages/impresion-ticket/impresion-ticket.module').then( m => m.ImpresionTicketPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
