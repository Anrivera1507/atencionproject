import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BeneficiosListadoPageRoutingModule } from './beneficios-listado-routing.module';

import { BeneficiosListadoPage } from './beneficios-listado.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BeneficiosListadoPageRoutingModule
  ]
})
export class BeneficiosListadoPageModule {}
