import { AfterContentChecked, AfterContentInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { BeneficioService } from 'src/app/services/beneficio.service';


@Component({
  selector: 'app-beneficios-listado',
  templateUrl: './beneficios-listado.page.html',
  styleUrls: ['./beneficios-listado.page.scss'],
})
export class BeneficiosListadoPage {

  constructor(private router: Router, public _beneficios: BeneficioService) {
   
   }
   
  cargarBeneficioDetalle(){
this.router.navigate(['beneficios-detalle']);
    }
}
