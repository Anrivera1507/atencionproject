import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BeneficiosListadoPage } from './beneficios-listado.page';

const routes: Routes = [
  {
    path: '',
    component: BeneficiosListadoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BeneficiosListadoPageRoutingModule {}
