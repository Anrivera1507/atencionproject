import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ImpresionTicketPageRoutingModule } from './impresion-ticket-routing.module';

import { ImpresionTicketPage } from './impresion-ticket.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImpresionTicketPageRoutingModule
  ],
  declarations: [ImpresionTicketPage]
})
export class ImpresionTicketPageModule {}
