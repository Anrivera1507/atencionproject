import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-impresion-ticket',
  templateUrl: './impresion-ticket.page.html',
  styleUrls: ['./impresion-ticket.page.scss'],
})
export class ImpresionTicketPage implements OnInit {

  constructor(private router: Router) { }
  usuario = {
    nombre: 'Juan',
    apellido: 'Pérez'
  }
  ngOnInit() {
  }

  regresarInicio(){
    this.router.navigate(['beneficios-listado']);
  }
  imprimirFechaActual(){
    // Create a date object with the current time
    let now: Date = new Date();
    // Create an array with the current month, day and time
    let date: Array<String> = [ String(now.getMonth() + 1), String(now.getDay()), String(now.getFullYear()) ];
    // Create an array with the current hour, minute and second
    let time: Array<String> = [ String(now.getHours()), String(now.getMinutes()), String(now.getSeconds())];
    // Return the formatted string
    return { 
        date: date.join("/"),
        time: time.join(":")
    };
}

imprimirTicker(){
  console.log("Imprimiendo ticket :D");
}
}
