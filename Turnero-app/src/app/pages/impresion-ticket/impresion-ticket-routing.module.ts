import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImpresionTicketPage } from './impresion-ticket.page';

const routes: Routes = [
  {
    path: '',
    component: ImpresionTicketPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ImpresionTicketPageRoutingModule {}
