import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage {


  userObj = {
    user: '',
    password: ''
  }
  constructor(
    public router: Router,
    public _user: UserService
    ) { }

  ngOnInit() {
  }
  authenticate() {
    this._user.authentication(this.userObj.user, this.userObj.password).subscribe(resp => {
      //console.log(resp);
     if (resp) {
        this.router.navigate(["/"]);
        Swal.fire({
          title: 'Bienvenid@!',
          text: resp.usuario,
          icon: 'success',
          confirmButtonText: 'Entendido',
          confirmButtonColor: '#111c4e',
          position: 'center'
        }).then(function (result) { })
      } else {
        Swal.fire({
          title: 'Aviso',
          text: 'Usuario o contraseña inválidos',
          icon: 'warning',
          confirmButtonText: 'Entendido',
          confirmButtonColor: '#e08729',
          position: 'center'
        }).then(function (result) { })
      }
    });
  }
}
