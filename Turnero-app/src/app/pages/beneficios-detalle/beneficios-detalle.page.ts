import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-beneficios-detalle',
  templateUrl: './beneficios-detalle.page.html',
  styleUrls: ['./beneficios-detalle.page.scss'],
})
export class BeneficiosDetallePage implements OnInit {
  requerimiento1 : boolean;

  habilitarBtn: boolean = true;
  constructor( public alertController: AlertController,
    private router: Router) { }

  ngOnInit() {
  }

  requerimientoCompletado(){
   // console.log('Nuevo estado:' + this.requerimiento1);
    if(this.requerimiento1==true){
      this.habilitarBtn = false
    }else{
      this.habilitarBtn = true
    }
  }

  confirmarGenerarTicket() {

    this.alertController.create({
      header: 'Generacion de Ticket',
      message: '¿Desea crear un numero de ticket para...?',
      buttons: [
        {
          text: 'Si',
          handler: () => {
            console.log('Generar Ticket');
            this.router.navigate(['impresion-ticket'])
          }
        },
        {
          text: 'No',
          handler: () => {
            console.log('Regresar a beneficios...');
          }
        }
      ]
    }).then(res => {
      res.present();
    });

  }

}
