import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SafePipe } from './safe.pipe';
import { FormsModule } from '@angular/forms';
import { JwtInterceptor } from './misc/jwt.interceptor';
import { UserService } from './services/user.service';

//Importacion de dependencias de socket Io 
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { environment } from 'src/environments/environment';
import { BeneficioService } from './services/beneficio.service';



//variable de configuracion para poder conectarnos al servidor
const config: SocketIoConfig = { url: environment.apiNodeLocal, options: {transports : ['websocket']} };

 

@NgModule({
  declarations: [AppComponent, SafePipe],
  entryComponents: [],
  imports: [BrowserModule,HttpClientModule, IonicModule.forRoot(), AppRoutingModule, FormsModule, SocketIoModule.forRoot(config)],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, {provide: LocationStrategy, useClass: HashLocationStrategy}, {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }, UserService, BeneficioService],
  bootstrap: [AppComponent],
})
export class AppModule {}
