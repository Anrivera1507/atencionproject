import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { BeneficiosListadoPage } from '../pages/beneficios-listado/beneficios-listado.page';
import { BeneficiosDetallePageModule } from '../pages/beneficios-detalle/beneficios-detalle.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule, BeneficiosDetallePageModule
  ],
  declarations: [HomePage, BeneficiosListadoPage]
})
export class HomePageModule {}
