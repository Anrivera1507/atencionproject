import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';
import { BeneficiosListadoPage } from '../pages/beneficios-listado/beneficios-listado.page';
const routes: Routes = [
  {
    path: '',
    component: BeneficiosListadoPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule {}
