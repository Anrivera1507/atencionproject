import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { Router } from '@angular/router';
import { BeneficioService } from 'src/app/services/beneficio.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})


export class HomePage implements OnInit {
 
  arrayPost: any;

  constructor(public _beneficios: BeneficioService) {}
  ngOnInit() {
    console.log("Cargando servicio de beneficios ************");
    this.listarBeneficios();//Llamamos a la función listarBeneficios cuando la vista se cargue
  }
  listarBeneficios() { //llamamos a la funcion de nuestro servicio.
    this._beneficios.obtenerCategoriasBeneficios("areasAtencion")
    .then(data => {
      this.arrayPost = data;
      console.log(this.arrayPost);
    });
  }
}
