import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BeneficioService {

  constructor(private http: HttpClient) { }

  obtenerCategoriasBeneficios(categoriaBeneficio: string){
    return new Promise(resolve=>{
      this.http.post(environment.ApiTurnero + "/administracion/catalogos/obtener", { "tipo" : categoriaBeneficio}).subscribe(data=>{
          resolve(data);
      },error=>{
        console.log(error);
      });
    });
  }

  consultarSocket(){
    return this.http.get(environment.apiNodeLocal + "/books").pipe(
      map((resp: any) => {
          return resp;
      }),
      catchError((err) => {
        return err 
            })
    );
  }
}
