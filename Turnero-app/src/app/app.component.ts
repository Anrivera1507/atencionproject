import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import { Socket } from 'ngx-socket-io';
import { ToastController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { UserService } from './services/user.service';
import { BeneficioService } from './services/beneficio.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  encapsulation: ViewEncapsulation.None
})


export class AppComponent implements OnInit {
  url: SafeResourceUrl;
  dark = false;
  icono = "";
  conexionServidorEstado: boolean;
  mensajeConexion: string = "";
  constructor(public sanitizer: DomSanitizer, private socket: Socket, private toastCtrl: ToastController, public _user: UserService, public _beneficios: BeneficioService
  ) {
    //this.url = sanitizer.bypassSecurityTrustResourceUrl('http://192.168.3.12:8090/#/veterano-buscar');
    this.url = sanitizer.bypassSecurityTrustResourceUrl(environment.URLSivetRegistro);
    this.cambiarTema(this.dark);
  }

  ngOnInit() {
    this.conectarSocket();
    this.cargarTokenRegistro();
   // this.probarSocket();
  }

  conectarSocket() {
    //Conexion al WebSocket
    this.socket.connect();

    //Captura de error si conexion al WebSocket no es posible
    this.socket.on('connect_error', () => {
      this.mostrarToast('Se perdio la conexion al servidor');
      this.mensajeConexion = "Desconectado";
      this.conexionServidorEstado = false;
    });

    //Conexion al SOCKET con evento connection-stabilished
    this.socket.fromEvent('connection-stabilished').subscribe(data => {
      if (data['event'] === 'server-running') {
        this.mostrarToast('Se establecio conexion al servidor');
        this.mensajeConexion = "Conectado";
        this.conexionServidorEstado = true;
      }
    });

    this.socket.on('websocket-endpoint-response', (data) => {
     console.log(data);
    });

    
  }

  async mostrarToast(msg: string) {
    let toast = await this.toastCtrl.create({
      message: msg,
      position: 'bottom',
      duration: 2000,
      buttons: [
        {
          icon: 'close-outline',
          role: 'cancel'
        }
      ]
    });
    toast.present();
  }

  cambiarTema(tema: boolean) {
    if (tema == true) {
      this.icono = "moon-outline";
    } else {
      this.icono = "sunny-outline";
    }
  }

  cargarTokenRegistro() {
    this._user.refreshIframe(environment.URLSivetRegistro, 'the-iframe');
  }

  probarSocket() {
   this._beneficios.consultarSocket().subscribe(resp =>{
     console.log(resp);
   });
  }
}