import { HttpHeaders } from "@angular/common/http";


export const header = new HttpHeaders({
    'content-type'                : 'application/json',
    'Authorization'               : "Bearer "+localStorage.getItem('token')
  })
