// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  initialRoute: 'http://50.50.50.28',
  //initialRoute: 'http://192.168.3.12:8090/#/dashboard',
  apiSivet: 'http://192.168.3.10:3001',
  apiNodeLocal: 'http://192.168.3.24:3001',
  URLSivetRegistro: 'http://192.168.3.24:4200',
  URLApiStatesProd: 'http://apis.inabve.gob.sv/v1/api_states/',
  ApiTurnero: 'http://192.168.254.3:3001',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
