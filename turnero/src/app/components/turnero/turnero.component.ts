import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { TurnosService } from '../../services/turnos.service';
import { Socket } from 'ngx-socket-io';
import { TicketModel } from 'src/app/models/turnos.model';

@Component({
  selector: 'app-turnero',
  templateUrl: './turnero.component.html',
  styleUrls: ['./turnero.component.css']
})
export class TurneroComponent implements OnInit, OnDestroy {

  codigoArea : string = localStorage.getItem('codigoArea') !
  cuenta : number = 1;
  cuentaLlamada : any = 0
  obj : object = {};
  blinkColor: string = 'green'
  blinkTurno: boolean = false
  showInDisplay: boolean = false
  isLoading : number = 0;

  constructor(
    public app : AppComponent,
    private turnosServ : TurnosService,
    private router : Router,
    private socket : Socket
  ) { }

  ngOnInit(): void {
    this.listaTickets = []
    this.isLoading = 0
  }

  ngOnDestroy(): void {
    this.socket.disconnect() 
  }

  ngAfterViewInit(): void {
    this.iniciarSocket()
    this.cuentaLlamada = setInterval(() => { 
      this.obtenerTicketsSocket()
    }, 5000);
  }

    listaTickets  : TicketModel[] = [];
    ticketModel   : TicketModel = new TicketModel(); 
    display       : boolean = false;
    
    llamada ( item : TicketModel ){
      let regex = /(\d+)/g;
      let cadena = item.codigo
      console.log(item.nombreCompleto);
      console.log(item.descripcion);
      console.log( cadena.replace(/^(0+)/g, ''))
        const msg = new SpeechSynthesisUtterance(
          //`Número .... ${item.codigo } ,..... pase a ventanilla ..., ${ item.escritorio }`
          `${item.nombreCompleto } ,..... pase a ... ventanilla ....... ${ item.escritorio }`
        );
        msg.lang = 'es-US';
        window.speechSynthesis.speak(msg);

        this.ticketModel = item
        // this.ticketModel = this.listaTickets[0]
        this.showInDisplay = true
        let _this = this
        setTimeout(()=>{
          _this.showInDisplay = false
        }, 4000)
        this.blinkColorAndTime()
    }

    blinkColorAndTime(){
      let _this = this
      if (this.showInDisplay) {
        setTimeout(()=>{
          _this.blinkTurno = !_this.blinkTurno
          this.blinkColorAndTime()
        }, 500)
      }
    }

    obtenerVentanilla ( codigoArea : string ){

      this.turnosServ.obtenerVentanilla( codigoArea ).subscribe(
        resp => {
          console.log(resp.data);
          this.listaTickets = resp.data
        },
        (err) => {
          console.log("Error");
        },()=>{

        })
    }

    cerrar(){
      localStorage.clear();
      sessionStorage.clear();
      clearInterval(this.cuentaLlamada);
      this.socket.disconnect() 
      this.router.navigate([""]);
    }

    iniciarSocket(){
      this.socket.connect()
    }

    obtenerTicketsSocket() {
      this.socket.emit('llamada-tickets-monitor', { "id_area" : this.codigoArea });
      console.log("conectando");
      this.socket.fromEvent<any>('connection-stabilished').subscribe( 
        lista  => {
          console.log(this.codigoArea);
          if (this.listaTickets.length < lista.data.length) {
            console.log(lista.data);
            this.listaTickets = lista.data
            this.ticketModel = this.listaTickets[0]
            this.llamada( this.ticketModel )
          }
        });
    }

}
