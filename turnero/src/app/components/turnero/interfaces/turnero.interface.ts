export interface Turno{

    id          : number, 
    nombre      : string, 
    lugar       : string, 
    numeroTurno : number, 
    numeroLugar : number
} 

export interface Ticket{
    descripcion     : string,
    id              : string,
    idEstado        : string,
    idArea          : string,
    idTecnico       : string,
    idVeterano      : string,
    codigo          : string,
    numeroLlamada   : string,
    fechaInicio     : string,
    fechaFin        : string,
    escritorio      : string,
    creadoEn        : string,
    creadoPor       : string,
    modificadoEn    : string,
    modificadoPor   : string,
    activo          : string,
    nombreCompleto  : string
}

export interface AreasAtencion{
    id              : string,
    idRelacion      : string,
    tipo            : string,
    codigo          : string,
    descripcion     : string,
    creadoEn        : string,
    creadoPor       : string,
    modificadoEn    : string,
    modificadoPor   : string,
    activo          : string
}