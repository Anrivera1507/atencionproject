import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TurnosService } from 'src/app/services/turnos.service';
import { AreasAtencion } from '../turnero/interfaces/turnero.interface';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-config-turnos',
  templateUrl: './config-turnos.component.html',
  styleUrls: ['./config-turnos.component.css']
})
export class ConfigTurnosComponent implements OnInit {

  constructor(
    private turnoServ : TurnosService,
    private router    : Router
  ) { }

  ngOnInit(): void {
    this.cargarAreasAtencion()
  }

  AreasAtencion : AreasAtencion [] = [];
  codigoArea : string = '';

  cargarAreasAtencion(){
    this.turnoServ.obtenerAreasAtencion().subscribe(
      (resp) =>{
        this.AreasAtencion = resp
        console.log(this.AreasAtencion);
      },
      (err) => {
        console.log(err);
      }
    )
  }

  guardarAreaAtencion( item : AreasAtencion){
    
    localStorage.setItem('codigoArea', item.id)
    this.codigoArea = localStorage.getItem('codigoArea') !
    console.log( localStorage.getItem('codigoArea'));
    if(this.codigoArea != ''){
      this.redirigirMonitor( item.descripcion )
    }
  }

  redirigirMonitor( descripcion : string ){
    let _this = this;
    Swal.fire({
      title:'Aviso',
      text: `Seguro de ingresar a turno del area ${ descripcion }`,
      showCancelButton: true,
      confirmButtonText:'Sí',
      cancelButtonText:'No, cancelar',
      confirmButtonColor: 'darkgreen',
      cancelButtonColor: '#808080',
    }).then(function ( resp )
    {
      if( resp.value ){
        _this.router.navigate(["/monitor-turnos"]);
        Swal.fire({
          title:'Bienvenid@!',
          icon:'success',
          confirmButtonText:'Entendido',
          confirmButtonColor: 'darkgreen'
        }).then(function ()
        {
          console.log();
              
        })
      }
          
    })
    
  }
}
