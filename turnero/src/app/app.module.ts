import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TurneroComponent } from './components/turnero/turnero.component';
import { HttpClientModule} from '@angular/common/http';
import { ConfigTurnosComponent } from './components/config-turnos/config-turnos.component'
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { environment } from 'src/environments/environment';
import { FormsModule } from '@angular/forms';
import {DialogModule} from 'primeng/dialog';

const config: SocketIoConfig = { url: environment.urlApi, options: {transports : ['websocket']} };

@NgModule({
  declarations: [
    AppComponent,
    TurneroComponent,
    ConfigTurnosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DialogModule,
    FormsModule,
    HttpClientModule,
    SocketIoModule.forRoot(config)
  ],
  exports:[
    TurneroComponent,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
