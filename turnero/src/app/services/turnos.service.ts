import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AreasAtencion } from '../components/turnero/interfaces/turnero.interface';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TurnosService {

  constructor( 
    private http : HttpClient,
    private router : Router
  )
  { 
    console.log('servio de turnos');
  }
  private _headers        : HttpHeaders = new HttpHeaders({ 'content-type' : 'application/json' });

  private _areas : AreasAtencion = {
    id              : '',
    idRelacion      : '',
    tipo            : '',
    codigo          : '',
    descripcion     : '',
    creadoEn        : '',
    creadoPor       : '',
    modificadoEn    : '',
    modificadoPor   : '',
    activo          : ''
  }
  // private _areas : AreasAtencion [] = [];
  
  cargarDatos(){
    const url = 'http://regres.in/api/users';
    return this.http.get( url );
  }


  public buscar(objParam: any): Observable<any> { 
    return this.http.post<any>(environment.urlApi+'/registros/veteranos/buscar',objParam, {'headers':this._headers})
    .pipe(
            map((resp: any) => { 
                return resp;
            }),
            catchError(err => {
                console.log(err)  
                return err;
            })
        )
  }

  public obtenerVentanilla( codigoArea : string ): Observable<any> { 
    return this.http.post<any>(environment.urlApi + '/ventanillas/tickets/monitor', { "id_area" : codigoArea }, {'headers' : this._headers})
    .pipe(
      map((resp: any) => { 
        return resp;
      }),
      catchError(err => {
        console.log(err)  
          return err;
          })
      )
  }

  obtenerAreasAtencion(){
    return this.http.post(environment.urlApi + "/administracion/catalogos/obtener", {"tipo":"areasAtencion"} ,{'headers': this._headers})
      .pipe(
        map((resp: any) => {          
          if (resp.data ) {
            return resp.data;
          } else {
            return null
          }
        }),
        catchError((err) => {
          return err ;
        })
      );
  }
  
}
