export const ImgAreasAtencion = [
    { IDAREA : '14066', CODIGO : 'C', IMGSRC : '../../../assets/images/creditos-img.svg' },
    { IDAREA : '14067', CODIGO : 'S', IMGSRC : '../../../assets/images/sicme-image.svg' },
    { IDAREA : '14068', CODIGO : 'P', IMGSRC : '../../../assets/images/cinabve-img.svg' },
    { IDAREA : '14069', CODIGO : 'V', IMGSRC : '../../../assets/images/becas-img.svg' },
    { IDAREA : '14070', CODIGO : 'R', IMGSRC : '../../../assets/images/registros-imgs.svg' },
]